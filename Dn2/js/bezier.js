function Point2f(x, y) {
	this.x = x;
	this.y = y;
}

Point2f.print = function(x) {
	document.write("x: " + x.x + " y: " + x.y + "<br>");
}

function BezierCurve(context, color, precision) {
	this.points = new Array();
	this.color = color;
	this.t = precision;
	this.casteljau = true;
	this.ctx = context;
	this.bezierMat = new Matrix4f(
							new Vector4f(1, -3, 3, -1),
							new Vector4f(0, 3, -6, 3),
							new Vector4f(0, 0, 3, -3),
							new Vector4f(0, 0, 0, 1)
							);

}

BezierCurve.addPoint = function(o, p) {
	o.points.push(p);
}

BezierCurve.drawPoints = function(o) {
	// draw points
	var offset = 4.0;
	for (i = 0; i < o.points.length; i++) {
		if (i != 0 && i % 3 != 0) {
			o.ctx.beginPath();
			o.ctx.arc(o.points[i].x - offset, o.points[i].y - offset, offset, 0, 2 * Math.PI);
			o.ctx.strokeStyle = o.color;
			o.ctx.stroke();
		}
		else {
			o.ctx.fillStyle = o.color;
			o.ctx.fillRect(o.points[i].x - offset, o.points[i].y - offset, 8, 8);
		}
	}
}

// draw curve using static precision
BezierCurve.staticDraw = function(o, pointMat) {
	var previous = new Point2f(-1, -1);
	var bool = false;

	var mat = Matrix4f.multiply(pointMat, o.bezierMat);
	for (i = 0; i <= 1.0; i += o.t) {
		var tMat = new Matrix4f(
					 new Vector4f(1,     0, 0, 0),
					 new Vector4f(i,     0, 0, 0), 
					 new Vector4f(i*i,   0, 0, 0),
					 new Vector4f(i*i*i, 0, 0, 0)
					);
	
		var mat2 = Matrix4f.multiply(mat, tMat);
		var point = new Point2f(mat2.x.x, mat2.y.x);
		
		if (previous.x != -1) {
			o.ctx.beginPath();
			o.ctx.moveTo(previous.x, previous.y);
			o.ctx.lineTo(point.x, point.y);
			o.ctx.strokeStyle = o.color;
			o.ctx.stroke();
		}

		previous = point;
	}
}

function subdivide(points) {
   var firstMidpoints = midpoints(points);
   var secondMidpoints = midpoints(firstMidpoints);
   var thirdMidpoints = midpoints(secondMidpoints);

   return [[points[0], firstMidpoints[0], secondMidpoints[0], thirdMidpoints[0]],
		  [thirdMidpoints[0], secondMidpoints[1], firstMidpoints[2], points[3]]];
}

function midpoints(pointList) {
	var midpoint = function(p, q) {
	   return new Point2f((p.x + q.x) / 2.0, (p.y + q.y) / 2.0);
	};

	var midpointList = new Array(pointList.length - 1);
	for (var i = 0; i < midpointList.length; i++) {
	   midpointList[i] = midpoint(pointList[i], pointList[i+1]);
	}

	return midpointList;
}

function isFlat(curve) {
   var tol = 10;
 
   var ax = 3.0*curve[1].x - 2.0*curve[0].x - curve[3].x; ax *= ax;
   var ay = 3.0*curve[1].y - 2.0*curve[0].y - curve[3].y; ay *= ay;
   var bx = 3.0*curve[2].x - curve[0].x - 2.0*curve[3].x; bx *= bx;
   var by = 3.0*curve[2].y - curve[0].y - 2.0*curve[3].y; by *= by;
 
   return (Math.max(ax, bx) + Math.max(ay, by) <= tol);
}

// draw curve with Casteljau's algorithm
// source: http://jeremykun.com/tag/de-casteljau/
BezierCurve.drawCasteljau = function(o, points) {
	if (isFlat(points)) {
		o.ctx.beginPath();
		o.ctx.moveTo(points[0].x, points[0].y);
		o.ctx.lineTo(points[3].x, points[3].y);
		o.ctx.strokeStyle = o.color;
		o.ctx.stroke();
	} 
	else {
		var curves = subdivide(points);
		this.drawCasteljau(o, curves[0]);
		this.drawCasteljau(o, curves[1]);
	}
}

BezierCurve.drawCurve = function(o) {
	if (o.points.length == 0)
		return;
	
	this.drawPoints(o);
	
	// draw bezier's curves
	// var nOfCurves = [0,0,0,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7][o.points.length - 1];
	var nOfCurves = Math.floor((o.points.length - 1) / 3);
	var c = 0;
	var a = 0;
	while (c < nOfCurves) {
		if (o.casteljau == true) {
			var points = [o.points[a], o.points[a + 1], o.points[a + 2], o.points[a + 3]];
			this.drawCasteljau(o, points);
		}
		else {
			pointMat = new Matrix4f(
					new Vector4f(o.points[a].x, o.points[a + 1].x, o.points[a + 2].x, o.points[a + 3].x),
					new Vector4f(o.points[a].y, o.points[a + 1].y, o.points[a + 2].y, o.points[a + 3].y),
					new Vector4f(0, 0, 0, 0),
					new Vector4f(0, 0, 0, 0)
					);
		
			this.staticDraw(o, pointMat);
		}
		
		c++;
		a += 3;
	}
}