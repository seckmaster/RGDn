// ---- VECOTOR ----
function Vector4f(x, y, z, h) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.h = h; // = 0
}

Vector4f.negate = function(vector){
	vector.x = -vector.x;
	vector.y = -vector.y;
	vector.z = -vector.z;
	return vector;
}

Vector4f.add = function(v1, v2) {
	return new Vector4f(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.h);
}

Vector4f.scalarProduct = function(v1, sc) {
	return new Vector4f(v1.x * sc, v1.y * sc, v1.z * sc, v1.h);
}

Vector4f.dotProduct = function(v1, v2) {
	return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.h * v2.h);
}

Vector4f.crossProduct = function(v1, v2) {
	return new Vector4f(v1.y * v2.z - v1.z * v2.y,
						v1.z * v2.x - v1.x * v2.z,
						v1.x * v2.y - v1.y * v2.x,
						v1.h);
}

Vector4f.vLength = function(v) {
	return Math.sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

Vector4f.normalize = function(v) {
	var len = Vector4f.vLength(v);
	return new Vector4f(v.x / len, v.y / len, v.z / len, 1);
}

Vector4f.project = function(v1, v2) {
	return Vector4f.scalarProduct(v2, Vector4f.dotProduct(v1, v2) / Vector4f.dotProduct(v2, v2), 1);
}

Vector4f.cosPhi = function(v1, v2) {
	var len1 = Vector4f.vLength(v1);
	var len2 = Vector4f.vLength(v2);
	return Vector4f.dotProduct(v1, v2) / (len1 * len2);
}

Vector4f.print = function(v) {
	document.write("[" + parseFloat(v.x).toFixed(2) + " " + parseFloat(v.y).toFixed(2) + " " + parseFloat(v.z).toFixed(2) + " " + parseFloat(v.h).toFixed(2) + "]");
	document.write("<br>");
}

// ---- MATRIX ----
function Matrix4f(x, y, z, h) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.h = h;
}

Matrix4f.negate = function(m) {
	Vector4f.negate(m.x);
	Vector4f.negate(m.y);
	Vector4f.negate(m.z);
	return m;
}

Matrix4f.add = function(m1, m2) {
	return new Matrix4f(Vector4f.add(m1.x, m2.x),
						Vector4f.add(m1.y, m2.y),
						Vector4f.add(m1.z, m2.z),
						m1.h);
}

Matrix4f.transpose = function(m) {
	return new Matrix4f(new Vector4f(m.x.x, m.y.x, m.z.x, m.h.x),
						new Vector4f(m.x.y, m.y.y, m.z.y, m.h.y),
						new Vector4f(m.x.z, m.y.z, m.z.z, m.h.z),
						new Vector4f(m.x.h, m.y.h, m.z.h, m.h.h));
}

Matrix4f.multiplyScalar = function(m, sc) {
	return new Matrix4f(Vector4f.scalarProduct(m.x, sc),
						Vector4f.scalarProduct(m.y, sc),
						Vector4f.scalarProduct(m.z, sc),
						m.h);
}

Matrix4f.multiply = function(m1, m2) {
	m2t = Matrix4f.transpose(m2);
	return new Matrix4f(
	new Vector4f(Vector4f.dotProduct(m1.x, m2t.x), Vector4f.dotProduct(m1.x, m2t.y), Vector4f.dotProduct(m1.x, m2t.z), Vector4f.dotProduct(m1.x, m2t.h)),
	new Vector4f(Vector4f.dotProduct(m1.y, m2t.x), Vector4f.dotProduct(m1.y, m2t.y), Vector4f.dotProduct(m1.y, m2t.z), Vector4f.dotProduct(m1.y, m2t.h)),
	new Vector4f(Vector4f.dotProduct(m1.z, m2t.x), Vector4f.dotProduct(m1.z, m2t.y), Vector4f.dotProduct(m1.z, m2t.z), Vector4f.dotProduct(m1.z, m2t.h)),
	new Vector4f(Vector4f.dotProduct(m1.h, m2t.x), Vector4f.dotProduct(m1.h, m2t.y), Vector4f.dotProduct(m1.h, m2t.z), Vector4f.dotProduct(m1.h, m2t.h))
	);			
}

Matrix4f.identity = function() {
	return new Matrix4f(new Vector4f(1, 0, 0, 0),
						new Vector4f(0, 1, 0, 0), 
						new Vector4f(0, 0, 1, 0),
						new Vector4f(0, 0, 0, 1));
}

Matrix4f.empty = function() {
	return new Matrix4f(new Vector4f(0, 0, 0, 0),
						new Vector4f(0, 0, 0, 0), 
						new Vector4f(0, 0, 0, 0),
						new Vector4f(0, 0, 0, 1));
}

Matrix4f.print = function(m) {
	Vector4f.print(m.x);
	Vector4f.print(m.y);
	Vector4f.print(m.z);
	Vector4f.print(m.h);
	document.write("<br>");
	document.write("<br>");
}

// ---- TRANSFORMATION ----
var Transformation = new function() {
	var m = Matrix4f.identity();
	
	this.get = function() {
		return m;
	}

	this.translate = function(v) {
		var t = new Matrix4f(new Vector4f(1, 0, 0, v.x),
						  	 new Vector4f(0, 1, 0, v.y),
						  	 new Vector4f(0, 0, 1, v.z),
						  	 new Vector4f(0, 0, 0, 1));
		m = Matrix4f.multiply(m, t);
	}


	this.scale = function(v) {
		var t = new Matrix4f(new Vector4f(v.x, 0, 0, 0),
						  	 new Vector4f(0, v.y, 0, 0),
						  	 new Vector4f(0, 0, v.z, 0),
						  	 new Vector4f(0, 0, 0, 1));
		m = Matrix4f.multiply(m, t);
	}

	this.rotateX = function(f) {
		var cos = Math.cos(f);
		var sin = Math.sin(f);
		var t = new Matrix4f(new Vector4f(1, 0, 0, 0),
		  	  				 new Vector4f(0, cos, -sin, 0),
		  	  				 new Vector4f(0, sin, cos, 0),
		  	  				 new Vector4f(0, 0, 0, 1));
		m = Matrix4f.multiply(m, t);
	}

	this.rotateY = function(f) {
		var cos = Math.cos(f);
		var sin = Math.sin(f);
		var t = new Matrix4f(new Vector4f(cos, 0, sin, 0),
				  	 		 new Vector4f(0, 1, 0, 0),
				  	 		 new Vector4f(-sin, 0, cos, 0),
				  	 		 new Vector4f(0, 0, 0, 1));
		m = Matrix4f.multiply(m, t);
	}

	this.rotateZ = function(f) {
		var cos = Math.cos(f);
		var sin = Math.sin(f);
		var t = new Matrix4f(new Vector4f(cos, -sin, 0, 0),
					  	 	 new Vector4f(sin, cos, 0, 0),
					  	 	 new Vector4f(0, 0, 1, 0),
					  	 	 new Vector4f(0, 0, 0, 1));
		m = Matrix4f.multiply(m, t);
	}
	
	this.transformPoint = function(p) {
		return new Vector4f(Vector4f.dotProduct(p, m.x),
							Vector4f.dotProduct(p, m.y),
							Vector4f.dotProduct(p, m.z),
							Vector4f.dotProduct(p, m.h));
	}
	
	this.loadIdentity = function() {
		m = Matrix4f.identity();
	}
}

