
var PointManager = new function() {
	this.vectors = [];
	
	this.read = function(input) {
		var lines = $(document.getElementById(input)).val().split('\n');
		this.vectors = [];
		for(var i = 0;i < lines.length;i++){
		    var v = lines[i].split(' ');
		    this.vectors.push(new Vector4f(parseInt(v[1]), parseInt(v[2]), parseInt(v[3]), 1));
		}
	}
	
	this.write = function(output) {
		for (var i = 0; i < this.vectors.length; i++) {
			var s = $(document.getElementById(output)).val();
			var v_to_s = "[" + this.vectors[i].x + " "  + this.vectors[i].y + " "  + this.vectors[i].z + "]"; 
			$(document.getElementById(output)).val(s + v_to_s + "\n");
		}
	}
	
}

var TransformPoints = new function() {
	this.load = function(input) {
		PointManager.read(input);
	}
	
	this.write = function(out) {
		PointManager.write(out);
	}
	
	this.applyTransformations = function() {
		Transformation.loadIdentity();
		Transformation.translate(new Vector4f(1.25, 0, 0, 1));;
		Transformation.rotateZ(Math.PI / 3);
		Transformation.translate(new Vector4f(0, 0, 4.15, 1));
		Transformation.translate(new Vector4f(0, 3.14, 0, 1));
		Transformation.scale(new Vector4f(1.12, 1.12, 1, 1));
		Transformation.rotateY(5 * Math.PI / 8);
		
		//Matrix4f.print(Transformation.get());
		
		for (var i = 0; i < PointManager.vectors.length; i++) {
			var point = PointManager.vectors[i];
			point = Transformation.transformPoint(point);
			PointManager.vectors[i] = point;
		}
	}
}
